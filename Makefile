# Help
.SILENT:
.PHONY: help

help: ## Display this help
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

# Docker
start: ## start docker services in development environment
	@echo "--> Starting docker services"
	docker-compose up -d
down: ## stop and remove docker services
	@echo "--> Stopping docker services"
	docker-compose down
